package nl.utwente.di.celsius;

import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;




public class celsius extends HttpServlet {
	/**
	 * 
	 * 
	 */
	private celsiusF celsiusF;
	private static final long serialVersionUID = 1L;
	public void init() throws ServletException {
		celsiusF = new celsiusF();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    String docType =
	      "<!DOCTYPE HTML>\n";
	    String title = "celsius"; 
	    out.println(docType +
	                "<HTML>\n" +
	                "<HEAD><TITLE>" + title + "</TITLE>" +
	                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
	                		"</HEAD>\n" +
	                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
	                "<H1>" + title + "</H1>\n" +              
	                "  <P>temperature : " +
	                   request.getParameter("temperature") + "\n" +
	                "  <P> temperature in F': " +
	                   Double.toString(celsiusF.getConverted(request.getParameter("temperature"))) +
	                "</BODY></HTML>");
	}
}
