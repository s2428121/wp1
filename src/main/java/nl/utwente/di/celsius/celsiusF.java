package nl.utwente.di.celsius;

public class celsiusF {

	public double getConverted(String parameter) {
		double i = Integer.parseInt(parameter);
		double converted = (i * 9)/5 + 32;
		return converted;
	}

}
